package com.example.fizzbuzz;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class FizzBuzzTest {

    FizzBuzz fizzbuzz;

    @BeforeEach
    void setUp() {
        fizzbuzz = new FizzBuzz();
    }
    @Test
    void canStartFizzBuzz() {
        assertNotNull(fizzbuzz);
    }

    // if the number is divisible by 3 return "Fizz"
    @Test
    void numDivisableBy3_retFizz() {
        // Arrange
        String expected = "Fizz";
        // Act
        String actual = fizzbuzz.convert(3);
        // Assert
        assertEquals(expected, actual);
    }

    // if the number is divisibable by 5 return "Buzz"
    @Test
    void numDivisableBy5_retFizz() {
        // Arrange
        String expected = "Buzz";
        // Act
        String actual = fizzbuzz.convert(5);
        // Assert
        assertEquals(expected, actual);
    }

    // if the number is divisable by both 3 and 5, return "FizzBuzz"
    @Test
    void numDivisableBy3and5_retFizzBuzz() {
        // Arrange
        String expected = "FizzBuzz";
        // Act
        String actual = fizzbuzz.convert(15);
        // Assert
        assertEquals(expected, actual);
    }

    // otherwise return the number as String
    @Test
    void numNotDivisableBy3or5_retNum() {
        // Arrange
        String expected = "4";
        // Act
        String actual = fizzbuzz.convert(4);
        // Assert
        assertEquals(expected, actual);
    }
}
